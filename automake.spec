%global _configure_gnuconfig_hack 0
%global __requires_exclude ^perl\\(Automake::
%global __provides_exclude ^perl\\(Automake::

Name:           automake
Version:        1.17
Release:        1
Summary:        A tool for automatically generating Makefile.in files
License:        GPLv2+ and GFDL-1.3-only and Public Domain and MIT
URL:            http://www.gnu.org/software/automake/
Source0:        http://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz
Source1:        http://git.savannah.gnu.org/cgit/config.git/plain/config.sub
Source2:        http://git.savannah.gnu.org/cgit/config.git/plain/config.guess

BuildArch:      noarch

BuildRequires:  perl autoconf make help2man automake perl-generators texinfo
#for tests
BuildRequires: libtool gettext-devel flex bison vala
BuildRequires: cscope dejagnu sharutils gcc-gfortran

Requires:       autoconf perl(threads) perl(Thread::Queue)
Requires(post): info
Requires(preun):info

%package_help

%description
iAutomake is a tool for automatically generating Makefile.in files compliant
with the GNU Coding Standards. Automake requires the use of Autoconf.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -iv
cp %{SOURCE1} ./lib/config.sub
cp %{SOURCE2} ./lib/config.guess
%configure
%make_build

%install
%make_install

%check
make %{?_smp_mflags} check

%post help
/sbin/install-info %{_infodir}/automake.info.gz %{_infodir}/dir || :

%preun help
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/automake.info.gz %{_infodir}/dir || :
fi

%files
%doc README
%doc %{_docdir}/%{name}/*.tar.gz
%license COPYING* AUTHORS doc/automake.info
%{_bindir}/*
%{_datadir}/automake-*
%{_datadir}/aclocal-*
%exclude %{_infodir}/dir
%exclude %{_datadir}/aclocal

%files help
%doc THANKS NEWS
%{_infodir}/*.info*
%{_mandir}/man1/*

%changelog
* Mon Jul 15 2024 fuanan <fuanan3@h-partners.com> - 1.17-1
- update version to 1.17

* Wed Mar  6 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 1.16.5-5
- update config.guess  config.sub from upstream to support loongarch64

* Sat Oct 22 2022 yixiangzhike <yixiangzhike007@163.com> - 1.16.5-4
- fix py-compile-basedir.sh missing test

* Thu Jul 28 2022 fuanan <fuanan3@h-partners.com> - 1.16.5-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Bump release and Cleanup useless comments and License compliance rectification

* Tue Feb 15 2022 zoulin <zoulin13@h-partners.com> - 1.16.5-1
- update version to 1.16.5

* Tue Dec 28 2021 renhongxun <renhongxun@huawei.com> - 1.16.2-4
- bugfix about python 3.10

* Fri Jul 30 2021 panxiaohe <panxiaohe@huawei.com> - 1.16.2-3
- Support -fno-common in vala-mix2 test

* Thu May 27 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 1.16.2-2
- Fix test failed

* Thu Jul 16 2020 wangchen <wangchen137@huawei.com> - 1.16.2-1
- Update to 1.16.2

* Sat Oct 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.1-6
- Package Init
